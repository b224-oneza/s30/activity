db.fruits.aggregate([
    {$match: {onSale: true} },
    {$group: {_id: "$onSale", fruitOnSale: {$count: {}}}},
    {$project: {_id: 0}}

]);


db.fruits.aggregate([
    {$match: {stock: {$gt: 20}} },
    {$group: {_id: "$stock", enoughStack: {$count: {}}}},
    {$project: {_id: 0}}
]);


db.fruits.aggregate([
    { $match: {onSale: true} },
    { $group: {_id: "$supplier_id", avg_price: {$avg: "$price"} } }
]);


db.fruits.aggregate([
    { $match: {onSale: true} },
    { $group: {_id: "$supplier_id", max_price: {$max: "$price"} } }
]);

db.fruits.aggregate([
    { $match: {onSale: true} },
    { $group: {_id: "$supplier_id", min_price: {$min: "$price"} } }
]);